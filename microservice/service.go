// Copyright 2020 Simão Gomes Viana
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// package microservice is used to create and start microservices
// that can accept connections and handle requests
package microservice

import (
	"errors"
	"fmt"
	"net"
	"reflect"

	"gitlab.com/xdevs23/go-reflectutil/reflectutil"

	"gitlab.com/xdevs23/micropher/message"

	"gitlab.com/xdevs23/micropher/namespace"

	"gitlab.com/xdevs23/micropher/logger"

	"gitlab.com/xdevs23/micropher/comm"
)

type MicroService struct {
	Address    string
	namespaces map[namespace.Name]namespace.Namespace
}

func New(address string) *MicroService {
	return &MicroService{
		Address:    address,
		namespaces: map[namespace.Name]namespace.Namespace{},
	}
}

func (s *MicroService) Listen() error {
	listener, err := net.Listen("tcp", s.Address)
	if err != nil {
		return err
	}

	for err == nil {
		var conn net.Conn
		conn, err = listener.Accept()
		go s.onAccept(comm.FromConn(conn))
	}

	return err
}

func (s *MicroService) onAccept(comm *comm.Communicator) {
	for {
		readResult := comm.ReadMessage()
		if readResult.Err != nil {
			if readResult.Err.Error() == "EOF" {
				// Connection aborted, stop trying to read
				return
			}
			logger.Error("Error while reading message: %s", readResult.Err.Error())
		} else {
			go s.handleMessage(readResult, comm)
		}
	}
}

func (s MicroService) callProcedure(msg *message.Message) ([]interface{}, error) {
	nameSpace, found := s.namespaces[msg.Namespace]
	if !found {
		return nil, fmt.Errorf(`namespace "%s" does not exist`, msg.Namespace)
	}

	procedure, err := findMethodByName(msg.Procedure, nameSpace.ProcedureContainer)
	if err != nil {
		return nil, fmt.Errorf("in namespace \"%s\": %v", nameSpace.Name, err)
	}

	return reflectValuesAsInterfaces(procedure.Call(interfacesAsReflectValues(msg.Arguments))), nil
}

func (s MicroService) handleMessage(readResult *comm.MessageReadResult, comm *comm.Communicator) {
	returnValues, err := s.callProcedure(readResult.Message)
	if err != nil {
		logger.Error("Error while calling procedure: %s", err.Error())
		return
	}

	err = comm.WriteMessage(&message.Message{
		Result: message.ProcedureCallResult{
			ReturnValues: returnValues,
		},
	})
	if err != nil {
		logger.Error("Error while writing message: %s", err.Error())
	}
}

func reflectValuesAsInterfaces(reflectValues []reflect.Value) []interface{} {
	interfaces := make([]interface{}, len(reflectValues))
	for i := 0; i < len(interfaces); i++ {
		interfaces[i] = reflectValues[i].Interface()
	}
	return interfaces
}

func interfacesAsReflectValues(interfaces []interface{}) []reflect.Value {
	reflectValues := make([]reflect.Value, len(interfaces))
	for i := 0; i < len(reflectValues); i++ {
		reflectValues[i] = reflect.ValueOf(interfaces[i])
	}
	return reflectValues
}

func findMethodByName(name string, procedures interface{}) (*reflect.Value, error) {
	proceduresValue := reflectutil.EnsureSinglePointerValue(reflect.ValueOf(procedures))
	procedure := proceduresValue.MethodByName(name)
	if procedure.IsZero() {
		return nil, errors.New(fmt.Sprintf(`procedure "%s" does not exist`, procedure))
	}
	return &procedure, nil
}

func (s *MicroService) AddNamespace(name namespace.Name, receiverStruct interface{}) *MicroService {
	s.namespaces[name] = namespace.Namespace{
		Name:               name,
		ProcedureContainer: receiverStruct,
	}
	return s
}
