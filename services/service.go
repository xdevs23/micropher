// Copyright 2020 Simão Gomes Viana
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// package services provides a simple interface to communicate
// with other microservices
package services

import "gitlab.com/xdevs23/micropher/comm"

type ServiceName = string

type Service struct {
	Name    ServiceName
	Address string
	comm    *comm.Communicator
}
