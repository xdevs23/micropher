// Copyright 2020 Simão Gomes Viana
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package services

var services = map[ServiceName]*Service{}

func add(service *Service) {
	services[service.Name] = service
}

func remove(service *Service) {
	delete(services, service.Name)
}

func Connect(services ...*Service) error {
	var err error

	for _, service := range services {
		err = service.Connect(service.Address)
		if err != nil {
			return err
		}

		add(service)
	}

	return nil
}

func Get(name ServiceName) *Service {
	return services[name]
}
