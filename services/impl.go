// Copyright 2020 Simão Gomes Viana
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package services

import (
	"errors"
	"fmt"
	"net"

	"gitlab.com/xdevs23/micropher/namespace"

	"gitlab.com/xdevs23/micropher/comm"
	"gitlab.com/xdevs23/micropher/message"
)

type CallResult struct {
	Err    error
	Result interface{}
}

func (s *Service) Connect(address string) error {
	conn, err := net.Dial("tcp", address)
	if err != nil {
		return err
	}
	s.comm = comm.FromConn(conn)
	return nil
}

func (s *Service) Call(namespace namespace.Name, procedure string, arguments ...interface{}) chan *CallResult {
	result := make(chan *CallResult)
	go func() {
		result <- s.call(namespace, procedure, arguments...)
	}()
	return result
}

func (s *Service) call(namespace namespace.Name, procedure string, arguments ...interface{}) *CallResult {
	result := &CallResult{}

	result.Err = s.comm.WriteMessage(&message.Message{
		Namespace: namespace,
		Procedure: procedure,
		Arguments: arguments,
	})
	if result.Err != nil {
		return result
	}

	response := <-s.comm.ResponseChannel()
	if response.Err != nil {
		result.Err = response.Err
		return result
	}

	errString := response.Message.Result.Error
	if errString != nil {
		result.Err = errors.New(fmt.Sprintf("error trying to perform procedure call: %s", errString))
		return result
	}

	returnValues := response.Message.Result.ReturnValues
	if len(returnValues) == 1 {
		result.Result = returnValues[0]
	} else {
		result.Result = returnValues
	}

	return result
}

func (s *Service) CallOneWay(namespace namespace.Name, procedure string, arguments ...interface{}) error {
	err := s.comm.WriteMessage(&message.Message{
		Namespace: namespace,
		Procedure: procedure,
		Arguments: arguments,
	})
	if err != nil {
		return err
	}

	return nil
}
