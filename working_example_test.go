// Copyright 2020 Simão Gomes Viana
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"net"
	"testing"

	"gitlab.com/xdevs23/micropher/microservice"

	"gitlab.com/xdevs23/micropher/services"
)

type TestingStruct struct{}

func (ts *TestingStruct) Test(msg string) (string, bool, int) {
	return fmt.Sprintf("Hello, your message was: %s", msg), true, rand.Int()
}

func (ts *TestingStruct) Fibonacci(a, b, max int64) int64 {
	n := a + b
	if n >= max {
		return b
	}
	log.Println(n)
	return Fibonacci(b, n, max)
}

func (ts *TestingStruct) Add(a, b int64) int64 {
	return a + b
}

func Fibonacci(a, b, max int64) int64 {
	result := <-services.
		Get(serviceName).
		Call("best_namespace", "Fibonacci", a, b, max)
	return result.Result.(int64)
}

const serviceAddress = "localhost:6000"
const serviceName = "test_service"

func Prepare() error {
	go func() {
		err := microservice.New(serviceAddress).
			AddNamespace("best_namespace", TestingStruct{}).
			Listen()
		if err != nil {
			panic(err)
		}
	}()

	for {
		// Wait for port to be open
		if conn, err := net.Dial("tcp", serviceAddress); err == nil {
			_ = conn.Close()
			break
		}
	}

	err := services.Connect(
		&services.Service{
			Name:    serviceName,
			Address: serviceAddress,
		},
	)
	return err
}

func TestWorkingExample(t *testing.T) {
	err := Prepare()
	if err != nil {
		t.Error(err)
	}

	testOne(t, serviceName, "best_namespace", "Test", "very cool message")
	testOne(t, serviceName, "best_namespace", "Test", "another cool message")

	log.Println(Fibonacci(0, 1, 200000))
}

func testOne(t *testing.T, serviceName string, namespace string, procedure string, arguments ...interface{}) {

	callResult := <-services.
		Get(serviceName).
		Call(namespace, procedure, arguments...)

	if callResult.Err != nil {
		log.Println(callResult.Err)
		t.Error(callResult.Err)
	}

	jsonByt, _ := json.Marshal(callResult.Result)
	log.Println(string(jsonByt))
}

var isPrepared = false

func BenchmarkWorkingExample(b *testing.B) {
	if !isPrepared {
		err := Prepare()
		if err != nil {
			b.Fatal(err)
		}
		isPrepared = true
	}

	b.StartTimer()
	for n := 0; n < b.N; n++ {
		callResult := <-services.
			Get(serviceName).
			Call("best_namespace", "Add", n, b.N)
		if callResult.Err != nil {
			b.Fatal(callResult.Err)
		}
	}
}
