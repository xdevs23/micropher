module gitlab.com/xdevs23/micropher

go 1.14

require (
	github.com/mitchellh/mapstructure v1.3.2 // indirect
	github.com/vmihailenco/msgpack v4.0.4+incompatible
	gitlab.com/xdevs23/go-reflectutil v0.0.0-20200705105910-a23c149a2b8e
)
