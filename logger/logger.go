// Copyright 2020 Simão Gomes Viana
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package logger

var logger Logger = defaultLogger{}

type Logger interface {
	Debug(format string, v ...interface{})
	Info(format string, v ...interface{})
	Warn(format string, v ...interface{})
	Error(format string, v ...interface{})
}

func Debug(fmt string, v ...interface{}) {
	logger.Debug(fmt, v...)
}

func Info(fmt string, v ...interface{}) {
	logger.Info(fmt, v...)
}

func Warn(fmt string, v ...interface{}) {
	logger.Warn(fmt, v...)
}

func Error(fmt string, v ...interface{}) {
	logger.Error(fmt, v...)
}

func Set(customLogger Logger) {
	logger = customLogger
}
