// Copyright 2020 Simão Gomes Viana
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package logger

import (
	"fmt"
	"log"
)

type defaultLogger struct{}

func (d defaultLogger) Debug(format string, v ...interface{}) {
	log.Println("[DEBUG]", fmt.Sprintf(format, v...))
}

func (d defaultLogger) Info(format string, v ...interface{}) {
	log.Println("[INFO]", fmt.Sprintf(format, v...))
}

func (d defaultLogger) Warn(format string, v ...interface{}) {
	log.Println("[WARN]", fmt.Sprintf(format, v...))
}

func (d defaultLogger) Error(format string, v ...interface{}) {
	log.Println("[ERROR]", fmt.Sprintf(format, v...))
}
