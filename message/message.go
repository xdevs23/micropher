// Copyright 2020 Simão Gomes Viana
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// package message defines the structure of messages
package message

import "gitlab.com/xdevs23/micropher/namespace"

type ProcedureCallResult struct {
	Error        *string
	ReturnValues []interface{}
}

type Message struct {
	Namespace namespace.Name
	Procedure string
	Arguments []interface{}
	Result    ProcedureCallResult
}
