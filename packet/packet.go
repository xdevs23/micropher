// Copyright 2020 Simão Gomes Viana
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// package packet defines the structure and interface of a packet used
// to communicate between services
package packet

import "gitlab.com/xdevs23/micropher/message"

type Type uint8
type ProtocolVersion uint16
type EncryptionType uint16

const (
	typeMin Type = iota
	_            = iota - 2

	// Add your types here
	TypeMessage

	typeMax = iota - 3
)

const (
	VersionInvalid ProtocolVersion = iota
	Version1
)

const (
	EncryptionNone EncryptionType = iota

	encryptionMax = iota - 1
)

type Packet struct {
	Message message.Message
}

type Header struct {
	Version       ProtocolVersion
	Type          Type
	Encryption    EncryptionType
	PayloadLength int64
	Key           int64
}

func MakeHeader(payloadLength int64, key int64) *Header {
	return &Header{
		Version:       Version1,
		Type:          TypeMessage,
		PayloadLength: payloadLength,
		Key:           key,
	}
}

func (header *Header) IsValid() bool {
	switch {
	case
		header.Version != Version1,
		header.Type < typeMin || header.Type > typeMax,
		header.PayloadLength < 0,
		header.Encryption < EncryptionNone || header.Encryption > encryptionMax:
		return false
	}

	return true
}
