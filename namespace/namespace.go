// Copyright 2020 Simão Gomes Viana
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package namespace

type Name = string

type Namespace struct {
	Name               Name
	ProcedureContainer interface{}
}
