// Copyright 2020 Simão Gomes Viana
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package comm

import (
	"bytes"
	"encoding/binary"
	"errors"
	"fmt"
	"io"
	"net"

	"github.com/vmihailenco/msgpack"
	"gitlab.com/xdevs23/micropher/message"
	"gitlab.com/xdevs23/micropher/packet"
)

var lastKey int64
var keyRecvMap = map[int64]ConnectionEventReceiver{}
var keyRecvChanMap = map[int64]chan *MessageReadResult{}

type Communicator struct {
	conn net.Conn
	key  int64
}

func (c *Communicator) onMessageReceived(readResult *MessageReadResult) {
	c.ResponseChannel() <- readResult
}

func FromConn(conn net.Conn) *Communicator {
	lastKey++
	return &Communicator{conn: conn, key: lastKey}
}

func encodeMessage(msg *message.Message) ([]byte, error) {
	return msgpack.Marshal(msg)
}

func decodeMessage(payload []byte) (*message.Message, error) {
	msg := &message.Message{}
	return msg, msgpack.Unmarshal(payload, msg)
}

func (c *Communicator) writeHeader(payloadLength int64) error {
	return binary.Write(c.conn, binary.BigEndian, packet.MakeHeader(payloadLength, c.key))
}

func (c *Communicator) readHeader() (*packet.Header, error) {
	header := packet.Header{}
	err := binary.Read(c.conn, binary.BigEndian, &header)
	if err != nil {
		return nil, err
	}
	if !header.IsValid() {
		return nil, errors.New("header that has been read is invalid")
	}
	return &header, nil
}

func (c *Communicator) readPayload(header *packet.Header) ([]byte, error) {
	payload := make([]byte, header.PayloadLength)
	buf := bytes.NewBuffer(payload)
	buf.Reset()

	totalWritten, err := io.CopyN(buf, c.conn, header.PayloadLength)
	if err != nil {
		return nil, err
	}
	if totalWritten < header.PayloadLength {
		return nil, errors.New(fmt.Sprintf(
			"incomplete payload (read %d, expected %d)", totalWritten, header.PayloadLength))
	}
	if totalWritten > header.PayloadLength {
		return nil, errors.New(fmt.Sprintf(
			"payload larger than expected (read %d, expected %d)", totalWritten, header.PayloadLength))
	}

	return buf.Bytes(), nil
}

func (c *Communicator) writePayload(payload []byte) error {
	bytesWritten, err := c.conn.Write(payload)
	if err != nil {
		return err
	}

	if bytesWritten != len(payload) {
		return errors.New(fmt.Sprintf(
			"failed to write payload, not all %d bytes were written (written: %d)", len(payload), bytesWritten))
	}

	return nil
}

// TODO: add decrypt
// TODO: add encrypt

type MessageReadResult struct {
	header  *packet.Header
	Message *message.Message
	Err     error
}

func (c *Communicator) ReadMessage() *MessageReadResult {
	result := &MessageReadResult{}

	var header *packet.Header
	header, result.Err = c.readHeader()
	if result.Err != nil {
		return result
	}
	c.key = header.Key

	var payload []byte
	payload, result.Err = c.readPayload(header)
	if result.Err != nil {
		return result
	}

	result.Message, result.Err = decodeMessage(payload)
	if result.Err != nil {
		return result
	}

	result.header = header

	return result
}

func (c *Communicator) recordResponse() chan *MessageReadResult {
	readChan := make(chan *MessageReadResult)
	go func() {
		readChan <- c.ReadMessage()
	}()
	return readChan
}

func (c *Communicator) recordResponses() {
	if _, found := keyRecvMap[c.key]; !found {
		keyRecvMap[c.key] = c
		keyRecvChanMap[c.key] = make(chan *MessageReadResult)
		go func() {
			for {
				readResult := c.ReadMessage()
				if readResult.Err == nil {
					keyRecvMap[readResult.header.Key].onMessageReceived(readResult)
				}
			}
		}()
	}
}

func (c *Communicator) WriteMessage(message *message.Message) error {
	var err error

	c.recordResponses()

	encoded, err := encodeMessage(message)
	if err != nil {
		return err
	}

	if err = c.writeHeader(int64(len(encoded))); err != nil {
		return err
	}

	if err = c.writePayload(encoded); err != nil {
		return err
	}

	return nil
}

func (c *Communicator) ResponseChannel() chan *MessageReadResult {
	return keyRecvChanMap[c.key]
}
