package comm

type ConnectionEventReceiver interface {
	onMessageReceived(readResult *MessageReadResult)
}
