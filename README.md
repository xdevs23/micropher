
# micropher – Go Microservice RPC library (PoC)

This is a lightweight library for creating and interconnecting
microservices over a TCP connection.

It's not production ready and is in no way intended for production
use (as of now) as there is no quality control and maintenance.

## Specifications

| Specification | Value |
| --- | --- |
| Transport layer protocol | TCP |
| Protocol | micropher protocol ([structure](https://gitlab.com/xdevs23/micropher/-/blob/main/packet/packet.go))
| Encoding/Serialization | msgpack |

## Feature set

 * Asynchronous calls using go channels
 * Serialize as much as msgpack supports
 * Namespaces
 * Use your own logger (`logger.Set(yourLogger)`)

## TODOs

 * Use HTTP for establishing a connection
 * Add encryption implementation to protocol
 * Consider switching to a more efficient serialization method
 * Add broadcasts
 * Consider adding code generation to reduce self-written boilerplate (~3 LOC per call)
 * Remove header magic
 * Add connection management and/or connection pool
 * Add authentication
 * Add more tests
 * Write protocol/implementation specification

## License

This library and its source code is licensed under the 3-clause BSD license.

```
Copyright (c) 2020 Simão Gomes Viana

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

   1. Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
   2. Redistributions in binary form must reproduce the above
copyright notice, this list of conditions and the following disclaimer
in the documentation and/or other materials provided with the
distribution.
   3. Neither the name of the copyright holder nor the names of its
contributors may be used to endorse or promote products derived from
this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

```

Insert following text at the top of every source file:

```
// Copyright 2020 Simão Gomes Viana
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.
```

The binary form of this library contains code published by The Go Authors
which is covered by the [3-clause BSD license](https://golang.org/LICENSE).

Licenses of modules are obtainable by visiting or cloning the respective
module repository as listed in the `go.mod` file.

